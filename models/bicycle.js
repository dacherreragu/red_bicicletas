var Bicycle = function(id, color, model, location) {
    this.id = id;
    this.color = color;
    this.model = model;
    this.location = location;
}

Bicycle.prototype.toString = function() {
    return 'id: ' + this.id + ' | color: ' + this.color;
}

Bicycle.allBicycles = []; // Store the bicycles
Bicycle.add = function(bicycle) {
    Bicycle.allBicycles.push(bicycle); // Insert bicycle
}

Bicycle.findById = function(bicycleId) {
    var bicycle = Bicycle.allBicycles.find(x => x.id == bicycleId)

    if(bicycle)
        return bicycle
    else
        throw new Error(`No existe una bicicleta con el id ${bicycleId}`);
}

Bicycle.removeById = function(bicycleId) {
    // var bicycle = Bicycle.allBicycles.find(x => x.if == bicycleId)

    for(var i = 0; i < Bicycle.allBicycles.length; i++) {
        if(Bicycle.allBicycles[i].id == bicycleId) {
            Bicycle.allBicycles.splice(i, 1);

            break;
        }
    }
}

/* var a = new Bicycle(1, 'Red', 'Urban', [4.639599, -74.079091]);
var b = new Bicycle(2, 'White', 'Urban', [4.644473, -74.077815]);

Bicycle.add(a);
Bicycle.add(b); */

module.exports = Bicycle;