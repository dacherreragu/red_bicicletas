var map = L.map('main_map').setView([4.6459474, -74.0782042066352], 13); // Nemesio Camacho 'El Campin' Stadium. Bogota, Colombia

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
}).addTo(map);

// AJAX
$.ajax({
    dataType: "json",
    url: "api/bicycles",
    success: function(result) {
        console.log("Result:" + result);

        result.bicycles.forEach(function(bicycle){ // For each bicycle
            L.marker(bicycle.location, {title: bicycle.id}).addTo(map); // Add a marker
        })
    }
})

var redIcon = L.icon({
    iconUrl: '../images/red_icon.png',

    iconSize:     [90, 90], // Size of the icon
    iconAnchor:   [22, 94], // Point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -76] // Point from which the popup should open relative to the iconAnchor
});


L.marker([4.6459474, -74.0782042066352], 
    {
        title: "Estadio 'El Campín'",
        icon: redIcon
    }).addTo(map); // Nemesio Camacho 'El Campin' Stadium. Bogota, Colombia
L.marker([4.6492837, -74.0772872950584], 
    {
        title: "Movistar Arena",
        icon: redIcon
    }).addTo(map); // Movistar Arena. Bogota, Colombia
L.marker([4.6367499, -74.083237757545], 
    {
        title: "Universidad Nacional de Colombia, Sede Bogotá",
        icon: redIcon
    }).addTo(map); // National University of Colombia. Bogota, Colombia