var Bicycle = require('../../models/bicycle');

exports.bicycle_list = function(req, res) {
    res.status(200).json({
        bicycles: Bicycle.allBicycles
    });
}

exports.bicycle_create = function(req, res) {
    var location = [req.body.lat, req.body.lng];

    var bicycle = new Bicycle(req.body.id, req.body.color, req.body.model, location);

    Bicycle.add(bicycle);

    res.status(201).json({
        bicycle: bicycle
    });
}

exports.bicycle_update = function(req, res) {
    var bicycle = Bicycle.findById(req.params.id);
    var location = bicycle.location;

    if(req.body.color != "" && req.body.color != null) {
        bicycle.color = req.body.color;
    }
    if(req.body.model != "" && req.body.model != null) {
        bicycle.model = req.body.model;
    }
    if(req.body.lat != "" && req.body.lat != null) {
        location[0] = req.body.lat;
    }
    if(req.body.lng != "" && req.body.lng != null) {
        location[1] = req.body.lng;
    }
    bicycle.location = location;

    res.status(200).json({
        bicycle: bicycle
    });
}

exports.bicycle_delete = function(req, res) {
    Bicycle.removeById(req.body.id);

    res.status(204).send();
}