var Bicycle = require('../../models/bicycle');
var request = require('request');
var server = require('../../bin/www');

beforeEach(() => {
    Bicycle.allBicycles = [];
});

describe('Bicycle API', () => {
    describe('GET Bicycles /', () => {
        it('Status 200', () => {
            expect(Bicycle.allBicycles.length).toBe(0);
    
            var a = new Bicycle(1, 'Red', 'Urban', [4.639599, -74.079091]);
            Bicycle.add(a);
    
            request.get('http://localhost:3000/api/bicycles', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            })
        });
    });

    describe('POST Bicycles /create', () => {
        it('Status 201', (done) => {
            var headers = { 'content-type': 'application/json' };
            var body = '{ "id": 10, "color": "Gray", "model": "Urban", "lat": 4.644473, "lng": -74.077815 }';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/Bicycles/create',
                body: body
            }, function(error, response, body) {
                expect(response.statusCode).toBe(201);
                expect(Bicycle.findById(10).color).toBe("Gray");

                done();
            });
        });
    });

    describe('PATCH Bicycles /update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var body = '{ "color": "Red" }';
            var id = 10;

            var a = new Bicycle(10, 'White', 'Urban', [4.639599, -74.079091]);
            Bicycle.add(a);

            request.patch({
                headers: headers,
                url: `http://localhost:3000/api/Bicycles/${id}/update`,
                body: body
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicycle.findById(id).color).toBe("Red");

                done();
            });
        });
    });

    describe('DELETE Bicycles /delete', () => {
        it('Status 204', (done) => {
            var headers = { 'content-type': 'application/json' };
            var id = 10;
            var body = `{ "id": ${id} }`;

            var a = new Bicycle(10, 'White', 'Urban', [4.639599, -74.079091]);
            Bicycle.add(a);

            expect(Bicycle.allBicycles.length).toBe(1);

            request.delete({
                headers: headers,
                url: `http://localhost:3000/api/Bicycles/delete`,
                body: body
            }, function(error, response, body) {
                expect(Bicycle.allBicycles.length).toBe(0);
                expect(response.statusCode).toBe(204);

                done();
            });
        });
    });
});