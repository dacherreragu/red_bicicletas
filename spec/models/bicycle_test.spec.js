var Bicycle = require('../../models/bicycle');

beforeEach(() => {
    Bicycle.allBicycles = [];
});

describe('Bicycle.allBicycles', () => {
    it('Starts void.', () => {
        expect(Bicycle.allBicycles.length).toBe(0);
    });
});

describe('Bicycle.add', () => {
    it('Add one.', () => {
        expect(Bicycle.allBicycles.length).toBe(0);

        var a = new Bicycle(1, 'Red', 'Urban', [4.639599, -74.079091]);
        Bicycle.add(a);

        expect(Bicycle.allBicycles.length).toBe(1);
        expect(Bicycle.allBicycles[0]).toBe(a);
    });
});

describe('Bicycle.findById', () => {
    it('Must return the bicycle with id = 1.', () => {
        expect(Bicycle.allBicycles.length).toBe(0);
        
        var a = new Bicycle(1, 'Red', 'Urban', [4.639599, -74.079091]);
        var b = new Bicycle(2, 'White', 'Urban', [4.644473, -74.077815]);

        Bicycle.add(a);
        Bicycle.add(b);

        var targetBicycle = Bicycle.findById(1);

        expect(targetBicycle.id).toBe(1);
        expect(targetBicycle.color).toBe(a.color);
        expect(targetBicycle.model).toBe(a.model);
    });
});

describe('Bicycle.removeById', () => {
    it('Must delete the bicycle with id = 2.', () => {
        expect(Bicycle.allBicycles.length).toBe(0);
        
        var a = new Bicycle(1, 'Red', 'Urban', [4.639599, -74.079091]);
        var b = new Bicycle(2, 'White', 'Urban', [4.644473, -74.077815]);

        Bicycle.add(a);
        Bicycle.add(b);

        expect(Bicycle.allBicycles.length).toBe(2);

        Bicycle.removeById(2);

        expect(Bicycle.allBicycles.length).toBe(1);
    });
});